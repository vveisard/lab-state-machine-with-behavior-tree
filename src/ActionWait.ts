import { ResultCode, resultCodes, RunningAction } from "blueshell";
import { CharacterBlackboard } from "./CharacterBlackboard";
import { UpdateCharacterBehaviorEvent } from "./UpdateCharacterBehaviorEvent";

/**
 * Waits a number of executions.
 */
export class ActionWait extends RunningAction<CharacterBlackboard, UpdateCharacterBehaviorEvent> {

  public waitingGet(state: CharacterBlackboard) {
    return (this.getNodeStorage(state) as any).waiting;
  }

  public waitingSet(state: CharacterBlackboard, value: number) {
    (this.getNodeStorage(state) as any).waiting = value;
  }

  public isComplete(state: CharacterBlackboard): boolean {
    return (this.getNodeStorage(state) as any).waiting >= this._wait;
  }

  protected isCompletionEvent(event: UpdateCharacterBehaviorEvent, state: CharacterBlackboard): boolean {
    return this.isComplete(state);
  }

  protected onIncomplete(state: CharacterBlackboard, event: UpdateCharacterBehaviorEvent): ResultCode {
    console.log(`onIncomplete`);

    // increment waiting
    this.waitingSet(state, this.waitingGet(state) + 1)

    return this.isComplete(state) ?
      resultCodes.SUCCESS :
      resultCodes.RUNNING;
  }

  protected activate(state: CharacterBlackboard, event: UpdateCharacterBehaviorEvent): ResultCode {
    console.log(`activate`);

    this.waitingSet(state, 0);

    return resultCodes.RUNNING;
  }

  protected deactivate(state: CharacterBlackboard, event: UpdateCharacterBehaviorEvent, res: ResultCode) {
    console.log(`deactivate`);

    // reset the node
    this.resetNodeStorage(state);
  }

  constructor(name: string, private _wait: number) {
    super(name);

    if (_wait <= 0) throw `Invalid`;
  }

}