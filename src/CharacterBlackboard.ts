import { BlueshellState } from "blueshell";

/**
 * Instanced to each character.
 */
export class CharacterBlackboard implements BlueshellState {

  //#region BlueshellState

  errorReason?: Error

  public __blueshell: any;

  //#endregion

  //#region Properties

  public airborne: boolean;

  //#endregion

  constructor() {
    this.airborne = false;
  }
}