import { Character } from "./Character";

export class UpdateCharacterMachineEvent {

  public _character: Character;

  constructor(character: Character) {
    this._character = character;
  }
}