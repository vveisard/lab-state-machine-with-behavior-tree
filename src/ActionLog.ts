import { Action, ResultCode } from "blueshell";
import { UpdateCharacterBehaviorEvent } from "./UpdateCharacterBehaviorEvent";
import { CharacterBlackboard } from "./CharacterBlackboard";

/**
 * Behavior forlog
 */
export class ActionLog extends Action<CharacterBlackboard, UpdateCharacterBehaviorEvent>{
  onEvent(state: CharacterBlackboard, event: UpdateCharacterBehaviorEvent): ResultCode {
    console.log(this._text);

    return "SUCCESS";
  }

  constructor(private _text: string, name?: string | undefined) {
    super(name);
  }
}